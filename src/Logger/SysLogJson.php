<?php

/**
 * @file
 * Contains \Drupal\syslog\Logger\SysLogJson.
 */

namespace Drupal\syslog_json\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\syslog\Logger\SysLog;

// Syslog module may not be enabled, but we extend its class.
// Reference "core" since syslog module may be disabled and not discoverable.
include_once DRUPAL_ROOT . '/core/includes/module.inc';
include_once drupal_get_path('core', NULL) . '/modules/syslog/src/Logger/SysLog.php';

/**
 * Redirects logging messages to syslog, JSON encoded.
 */
class SysLogJson extends SysLog {
  /**
   * Constructs a SysLogJson object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LogMessageParserInterface $parser) {
    parent::__construct($config_factory, $parser);
    $this->config_factory = $config_factory;
    $this->config = $config_factory->get('syslog_json.settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function openConnection() {
    if (!$this->connectionOpened) {
      $facility = $this->config->get('facility');
      if ($facility === '') {
        $facility = defined('LOG_LOCAL0') ? LOG_LOCAL0 : LOG_USER;
      }
      $this->connectionOpened = openlog($this->config->get('identity'), LOG_NDELAY, $facility);
    }
  }

  public function log($level, $message, array $context = array()) {
    global $base_url;

    // Populate the message placeholders and then replace them in the message.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
    $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);

    // In general, it doesn't appear we have a good way in PHP of determining
    // the name of the currently-open logging facility, so we couldn't temporarily
    // change and then switch back.
    // @todo: Is there a way to log to a totally different facility?
    if (!\Drupal::moduleHandler()->moduleExists('syslog')) {
      $this->openConnection();
    }
    else {
      // We don't necessarily know which module will run first, but that's fine.
      // There doesn't appear to be much slowdown to openlog() more than once.
      // @see http://php.net/manual/en/function.openlog.php#112856
      $syslog = new SysLog($this->config_factory, $this->parser);
      $syslog->openConnection();
    }

    $entry = array(
      'base_url' => $base_url,
      'timestamp' => $context['timestamp'],
      'type' => $context['channel'],
      'ip' => $context['ip'],
      'request_uri' => $context['request_uri'],
      'referer' => $context['referer'],
      'uid' => $context['uid'],
      'link' => strip_tags($context['link']),
      'message' => strip_tags($message),
    );

    syslog($level, json_encode($entry));
  }

}
